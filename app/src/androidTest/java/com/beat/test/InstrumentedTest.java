package com.beat.test;

import com.beat.test.data.interactors.Provider;
import com.beat.test.data.models.BMarker;
import com.beat.test.data.models.ResultListener;
import com.beat.test.data.models.State;
import com.beat.test.data.models.Venue;
import com.beat.test.presenters.AddressPresenter;
import com.beat.test.presenters.SearchPresenter;
import com.beat.test.ui.screens.MapHostingScreen;
import com.beat.test.ui.screens.UiMap;
import com.google.android.gms.maps.model.LatLng;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class InstrumentedTest {

    @Mock
    MapHostingScreen screen;
    @Mock
    UiMap map;


    List<Venue> randomVenues = getRandomVenues();


    LatLng testPoint = new LatLng(37.99083, 23.720786);

    private AddressPresenter addressPresenter;
    private SearchPresenter searchPresenter;


    private Provider<List<Venue>> noVenuesProvider = new Provider<List<Venue>>() {
        @Override
        public void data(ResultListener<List<Venue>> listener) {

            listener.start();
            listener.success(Collections.<Venue>emptyList());

        }
    };


    private Provider<List<Venue>> randomVenuesProvider = new Provider<List<Venue>>() {
        @Override
        public void data(ResultListener<List<Venue>> listener) {

            listener.start();
            listener.success(randomVenues);

        }
    };

    private List<Venue> getRandomVenues() {
        List<Venue> venues = new ArrayList<>();

        SecureRandom secureRandom = new SecureRandom();

        for (int i = 0; i < 5; i++)
            venues.add(new Venue(String.valueOf(secureRandom.nextLong())
                    , "randomName " + i, new LatLng(23.43 + i, 45.36 + i), "randomAddress " + i, "food"));

        return venues;


    }

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void checkAddress() {
        addressPresenter = spy(new AddressPresenter(new Provider<String>() {
            @Override
            public void data(final ResultListener<String> listener) {
                listener.start();
                listener.success("papadiamanti 2");
            }
        }, screen, testPoint));

        doReturn("Searching").when(addressPresenter).string(R.string.searching);
        addressPresenter.address();
        verify(screen, times(1)).updateAddress("Searching");
        verify(screen, times(1)).updateAddress("papadiamanti 2");
        verify(screen, times(2)).updateAddress(anyString());
    }


    //no venues
    @Test
    public void checkNoVenuesSearch() {
        searchPresenter = spy(new SearchPresenter(noVenuesProvider, screen, map, testPoint));
        doReturn("Searching").when(searchPresenter).string(R.string.searching);
        doNothing().when(screen).showErrorMessage(any(Integer.class));

        searchPresenter.search();
        verify(screen, times(1)).showLoading("Searching");
        verify(screen, times(1)).hideLoading();
        verify(screen, times(1)).showErrorMessage(R.string.no_venues_available);
    }

    @Test
    public void checkVenuesSearch() {

        searchPresenter = spy(new SearchPresenter(randomVenuesProvider, screen, map, testPoint));
        doReturn("Searching").when(searchPresenter).string(R.string.searching);

        searchPresenter.search();
        verify(screen, times(1)).showLoading("Searching");
        verify(screen, times(1)).hideLoading();
        verify(screen, times(0)).showErrorMessage(anyInt());
        verify(map, times(5)).addPoi(Matchers.<BMarker>any());
        verify(screen, times(1)).showVenues(randomVenues);
        verify(screen, times(1)).transitToState(State.SEARCH);

    }


}
