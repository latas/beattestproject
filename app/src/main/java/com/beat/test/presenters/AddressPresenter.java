package com.beat.test.presenters;

import com.beat.test.R;
import com.beat.test.data.interactors.AddressProvider;
import com.beat.test.data.interactors.Provider;
import com.beat.test.data.models.ResultListener;
import com.beat.test.data.models.error.Error;
import com.beat.test.ui.screens.MapHostingScreen;
import com.google.android.gms.maps.model.LatLng;

/**
 * BeatTestProject-Android
 * <p>
 * Created by Antonis Latas on 9/15/2017
 */

public class AddressPresenter extends Presenter implements ResultListener<String> {
    final LatLng pos;
    final MapHostingScreen screen;
    final Provider<String> provider;

    public AddressPresenter(Provider<String> provider, MapHostingScreen screen, LatLng pos) {
        super(screen);
        this.pos = pos;
        this.screen = screen;
        this.provider = provider;
    }

    public AddressPresenter(MapHostingScreen screen, LatLng pos) {
        this(new AddressProvider(screen.context(), pos), screen, pos);
    }


    public void address() {
        provider.data(this);
    }


    @Override
    public void start() {
        screen.updateAddress(string(R.string.searching));
    }

    @Override
    public void success(String string) {
        screen.updateAddress(string);
    }

    @Override
    public void failure(Error error) {

    }
}
