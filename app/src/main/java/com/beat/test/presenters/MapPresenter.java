package com.beat.test.presenters;


import android.location.Location;

import com.beat.test.R;
import com.beat.test.data.interactors.MapProvider;
import com.beat.test.data.interactors.MyLocationProvider;
import com.beat.test.data.interactors.Provider;
import com.beat.test.data.models.error.Error;
import com.beat.test.data.models.error.ErrorCodes;
import com.beat.test.data.models.ResultListener;
import com.beat.test.data.models.State;
import com.beat.test.ui.screens.UiMap;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

public class MapPresenter extends Presenter {

    final UiMap map;
    boolean mapLoaded;
    final Provider<Location> myLocationProvider;
    final Provider<GoogleMap> mapProvider;


    //ctors
    public MapPresenter(UiMap map) {
        this(map, new MyLocationProvider(map.screen().context()), new MapProvider(map.hostedFragment()));
    }


    public MapPresenter(UiMap map, Provider<Location> myLocationProvider) {
        this(map, myLocationProvider, new MapProvider(map.hostedFragment()));
    }

    public MapPresenter(UiMap map, Provider<Location> myLocationProvider, Provider<GoogleMap> mapProvider) {
        super(map.screen());
        this.map = map;
        this.myLocationProvider = myLocationProvider;
        this.mapProvider = mapProvider;
    }

    Runnable mapLoadedRunnable = new Runnable() {
        @Override
        public void run() {

            map.centerToDefaultPoint(new LatLng(37.99083, 23.720786));
        }
    };


    ResultListener<Location> myLocationListener = new ResultListener<Location>() {
        @Override
        public void start() {
            map.screen().showLoading(string(R.string.locating));
        }

        @Override
        public void success(final Location location) {

            if (mapLoaded) {
                map.addMyPositionPoi(new LatLng(location.getLatitude(), location.getLongitude()));
                map.enableGestures();
            } else
                mapLoadedRunnable = new Runnable() {
                    @Override
                    public void run() {
                        map.addMyPositionPoi(new LatLng(location.getLatitude(), location.getLongitude()));
                        map.enableGestures();
                    }
                };

            map.screen().hideLoading();
            map.screen().transitToState(State.MAP);
        }

        @Override
        public void failure(Error error) {
            if (error.code() != ErrorCodes.unUthorizedLocationError)
                map.onLocationSetFailed(error.message());
            else {
                map.onNoLocationPermisionGranted();
            }
            map.screen().hideLoading();
        }
    };


    ResultListener<GoogleMap> mapLoadedListener = new ResultListener<GoogleMap>() {
        @Override
        public void start() {
            map.screen().showLoading(string(R.string.locating));
        }

        @Override
        public void success(GoogleMap gmap) {
            mapLoaded = true;
            map.onMapLoaded(gmap);
            mapLoadedRunnable.run();
            map.screen().hideLoading();
        }

        @Override
        public void failure(Error error) {

        }
    };


    public void loadMap() {
        mapProvider.data(mapLoadedListener);
    }

    public void myPositionUpdate() {
        myLocationProvider.data(myLocationListener);
    }

}
