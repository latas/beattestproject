package com.beat.test.presenters;

import com.beat.test.data.models.BMarker;
import com.beat.test.data.models.State;
import com.beat.test.ui.screens.UiMap;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import java.util.List;

/**
 * BeatTestProject-Android
 * <p>
 * Created by Antonis Latas on 9/15/2017
 */

public class MapGesturesPresenter extends Presenter {


    private final UiMap map;
    final State state;


    public MapGesturesPresenter(UiMap map, State state) {
        super(map.screen());
        this.map = map;
        this.state = state;
    }


    public MapGesturesPresenter(UiMap map) {
        this(map, State.MAP);
    }


    public void onMapIdle(GoogleMap googleMap) {
        if (state == State.SEARCH)
            return;

        map.updateAddress(googleMap.getProjection().getVisibleRegion().latLngBounds.getCenter());
        map.screen().showSearch();
    }

    public void onMapScrollStrarted(int reason) {
        if (state == State.SEARCH)
            return;
        if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {

            map.screen().hideSearch();
        }
    }

    public void onMarkerClick(Marker marker, List<BMarker> poisMarkers) {

        for (BMarker m : poisMarkers) {
            if (m.id().equals(marker.getId())) {
                m.select();
                map.onMarkerSelected(m);
            } else {
                m.unselect();
            }
        }
    }
}
