package com.beat.test.presenters;

import com.beat.test.R;
import com.beat.test.data.interactors.FourthSquareProvider;
import com.beat.test.data.interactors.Provider;
import com.beat.test.data.models.BMarker;
import com.beat.test.data.models.error.Error;
import com.beat.test.data.models.ResultListener;
import com.beat.test.data.models.SortedVenues;
import com.beat.test.data.models.State;
import com.beat.test.data.models.Venue;
import com.beat.test.data.models.error.NoVenuesAvailableError;
import com.beat.test.ui.screens.UiMap;
import com.beat.test.ui.screens.VenuesListScreen;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * BeatTestProject-Android
 * <p>
 * Created by Antonis Latas on 9/15/2017
 */

public class SearchPresenter extends Presenter implements ResultListener<List<Venue>> {
    final VenuesListScreen screen;
    final UiMap map;
    final LatLng point;
    final Provider<List<Venue>> provider;


    @Override
    public void start() {
        screen.showLoading(string(R.string.searching));
    }

    @Override
    public void success(List<Venue> venues) {
        screen.hideLoading();
        if (venues.size() == 0) {
            screen.showErrorMessage(new NoVenuesAvailableError().message());
            return;
        }


        screen.transitToState(State.SEARCH);
        map.transitToState(State.SEARCH);

        venues = new SortedVenues(venues, point).sort();
        screen.showVenues(venues);
        for (Venue v : venues) {
            map.addPoi(new BMarker(v));
        }
    }

    @Override
    public void failure(Error error) {
        screen.hideLoading();
        screen.showErrorMessage(error.message());
    }


    public SearchPresenter(VenuesListScreen screen, UiMap map, LatLng point) {
        this(new FourthSquareProvider(point), screen, map, point);
    }


    public SearchPresenter(Provider<List<Venue>> provider, VenuesListScreen screen, UiMap map, LatLng point) {
        super(screen);
        this.screen = screen;
        this.map = map;
        this.point = point;
        this.provider = provider;
    }


    public void search() {
        provider.data(this);

    }

}
