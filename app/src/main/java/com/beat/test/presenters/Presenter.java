package com.beat.test.presenters;

import com.beat.test.ui.screens.Screen;

public class Presenter {
    final Screen screen;

    public Presenter(Screen screen) {
        this.screen = screen;
    }

    public String string(int stringRes) {
        return screen.context().getString(stringRes);
    }

}
