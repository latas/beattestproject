package com.beat.test.presenters;

import com.beat.test.data.models.State;
import com.beat.test.ui.screens.MapHostingScreen;
import com.beat.test.ui.screens.UiMap;

/**
 * BeatTestProject-Android
 * <p>
 * Created by Antonis Latas on 9/15/2017
 */

public class StatePresenter extends Presenter {
    final MapHostingScreen screen;
    final UiMap map;
    State state;

    public StatePresenter(MapHostingScreen screen, UiMap map) {
        super(screen);
        state = State.INIT;
        this.screen = screen;
        this.map = map;
    }

    public void reportState(State state) {
        this.state = state;
        if (state == State.SEARCH) {
            screen.convertFloatingToDismiss();
            screen.hideAddressPanel();
            map.replaceUserPosInicator();
        } else if (state == State.MAP) {
            screen.showSearch();
            screen.convertFloatingToSearch();
            screen.clearVenues();
            map.updateAddress();
            map.showUserLocationCenteredPin();
            map.clearPois();

        }
        map.transitToState(state);
    }


}
