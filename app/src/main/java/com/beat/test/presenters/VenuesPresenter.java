package com.beat.test.presenters;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.widget.ImageView;

import com.beat.test.R;
import com.beat.test.data.models.Device;
import com.beat.test.data.models.Venue;
import com.beat.test.ui.activities.VenueDetailsActivity;
import com.beat.test.ui.screens.VenuesListScreen;

import java.util.List;


public class VenuesPresenter extends Presenter {


    final VenuesListScreen screen;
    final List<Venue> venues;

    public VenuesPresenter(VenuesListScreen screen, List<Venue> venues) {
        super(screen);
        this.screen = screen;
        this.venues = venues;
    }

    public void show(Venue v) {
        for (int i = 0; i < venues.size(); i++) {
            if (v.id().equals(venues.get(i).id())) {
                screen.scrollToItem(i);
            }
        }
    }

    public void onVenuePageScrolled(int position) {
        screen.onVenueSelected(venues.get(position));
    }


    public void onVenueClicked(int postition, ImageView venueImage) {
        Intent intent = new Intent(screen.context(), VenueDetailsActivity.class);

        intent.putExtra(string(R.string.venue_name_key), venues.get(postition).name());
        intent.putExtra(string(R.string.venue_address_key), venues.get(postition).address());
        intent.putExtra(string(R.string.venue_type_key), venues.get(postition).type());
        intent.putExtra(string(R.string.venue_image_key), venues.get(postition).photo());
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation((Activity) screen.context(), venueImage, string(R.string.venue_image_transition_name));
        if (new Device(screen.context()).isLollipop())
            screen.navigateToDetails(intent, options);
        else
            screen.navigateToDetails(intent);

    }
}
