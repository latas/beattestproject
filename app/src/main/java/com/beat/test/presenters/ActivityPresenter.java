package com.beat.test.presenters;

import com.beat.test.R;
import com.beat.test.ui.screens.ActivityScreen;

/**
 * BeatTestProject-Android
 * <p>
 * Created by Antonis Latas on 9/15/2017
 */

public class ActivityPresenter extends Presenter {
    final ActivityScreen screen;

    public ActivityPresenter(ActivityScreen screen) {
        super(screen);
        this.screen = screen;
    }

    public ActivityPresenter init() {

        switch (screen.code()) {
            case 0:
                screen.setToolbarIcon(R.drawable.logo);
                screen.setToolbarTitle("");
                screen.homeButtonStatus(false);
                break;
            case 1:
                screen.setToolbarIcon(0);
                screen.setToolbarTitle(string(R.string.selected_place));
                screen.homeButtonStatus(true);
                screen.homeButtonIcon(R.drawable.page_1);
        }

        return this;
    }


    public void onBackPressed() {
        screen.exit();
    }


    public void onHomeUpBtnPressed() {

        if (screen.code() == 1)
            screen.closeWithTransition();
        else screen.exit();


    }
}
