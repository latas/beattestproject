package com.beat.test.presenters;

import com.beat.test.data.models.VenueAssociated;
import com.beat.test.ui.VenueSelector;

public class VenueSelectionPresenter {


    VenueAssociated venueAssociated;

    public VenueSelectionPresenter(VenueAssociated venueAssociated) {
        this.venueAssociated = venueAssociated;
    }

    public void selectVenueOn(VenueSelector selector) {
        selector.select(venueAssociated);
        selector.center(venueAssociated);
    }


}
