package com.beat.test.ui.screens;

import com.beat.test.data.models.BMarker;
import com.beat.test.data.models.State;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

public interface UiMap {

    void addMyPositionPoi(LatLng latLng);


    void onLocationSetFailed(int message);

    void addPoi(BMarker marker);

    void onPoisReveivedFailed();

    UiMap init();

    MapHostingScreen screen();


    void onMapLoaded(GoogleMap googleMap);

    SupportMapFragment hostedFragment();

    void centerToDefaultPoint(LatLng latLng);

    void onNoLocationPermisionGranted();

    void updateUserLocation();

    void updateAddress(LatLng forPoint);

    LatLng center();

    void updateAddress();

    void transitToState(State state);

    void clearPois();

    void onMarkerSelected(BMarker marker);


    void replaceUserPosInicator();

    void enableGestures();

    void showUserLocationCenteredPin();
}
