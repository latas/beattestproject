package com.beat.test.ui.screens;

import android.widget.ImageView;

public interface MapHostingScreen extends VenuesListScreen {


    void requestLocationPermission();

    void updateAddress(String s);

    void showSearch();

    void hideSearch();

    void hideAddressPanel();

    public void clearVenues();

    void convertFloatingToDismiss();

    void convertFloatingToSearch();

    ImageView userPoiView();


}
