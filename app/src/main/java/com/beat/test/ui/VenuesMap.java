package com.beat.test.ui;

import android.view.View;
import android.widget.ImageView;

import com.beat.test.R;
import com.beat.test.data.models.BMarker;
import com.beat.test.data.models.State;
import com.beat.test.data.models.VenueAssociated;
import com.beat.test.presenters.AddressPresenter;
import com.beat.test.presenters.MapGesturesPresenter;
import com.beat.test.presenters.MapPresenter;
import com.beat.test.presenters.VenueSelectionPresenter;
import com.beat.test.ui.screens.MapHostingScreen;
import com.beat.test.ui.screens.UiMap;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

public class VenuesMap implements UiMap, VenueSelector {
    final SupportMapFragment mapFragment;
    GoogleMap googleMap;
    MapPresenter presenter;
    MapGesturesPresenter mapGesturesPresenter;
    MapHostingScreen screen;
    VenueSelector selector;
    Marker userMarker;
    List<BMarker> poisMarkers = new ArrayList<>();


    public VenuesMap(SupportMapFragment fragment, MapHostingScreen hostedScreen, VenueSelector selector) {
        this.mapFragment = fragment;
        this.screen = hostedScreen;
        this.selector = selector;
    }


    @Override
    public void addMyPositionPoi(LatLng latLng) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,
                13));
        screen().userPoiView().setVisibility(View.VISIBLE);
    }


    @Override
    public void onLocationSetFailed(int message) {
        screen.showErrorMessage(message);

    }

    @Override
    public void addPoi(BMarker marker) {
        poisMarkers.add(new BMarker(marker.venue(), googleMap.addMarker(new MarkerOptions()
                .position(marker.position()).icon(BitmapDescriptorFactory.fromResource(R.drawable.pin)))));

    }

    @Override
    public void onPoisReveivedFailed() {

    }

    @Override
    public VenuesMap init() {
        presenter = new MapPresenter(this);
        presenter.myPositionUpdate();
        presenter.loadMap();
        return this;
    }

    @Override
    public MapHostingScreen screen() {
        return screen;
    }


    @Override
    public void onMapLoaded(final GoogleMap googleMap) {
        this.googleMap = googleMap;
        mapGesturesPresenter = new MapGesturesPresenter(this);
    }

    @Override
    public SupportMapFragment hostedFragment() {
        return mapFragment;
    }

    @Override
    public void centerToDefaultPoint(LatLng latLng) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13));
    }

    @Override
    public void onNoLocationPermisionGranted() {
        screen.requestLocationPermission();
    }

    @Override
    public void updateUserLocation() {
        presenter.myPositionUpdate();
    }

    @Override
    public void updateAddress(LatLng forPoint) {
        new AddressPresenter(screen(), forPoint).address();
    }

    @Override
    public LatLng center() {
        return this.googleMap.getProjection().getVisibleRegion().latLngBounds.getCenter();

    }

    @Override
    public void updateAddress() {
        updateAddress(center());
    }

    @Override
    public void transitToState(State state) {
        mapGesturesPresenter = new MapGesturesPresenter(this, state);
    }

    @Override
    public void clearPois() {
        googleMap.clear();
        poisMarkers.clear();
    }

    @Override
    public void onMarkerSelected(BMarker marker) {
        new VenueSelectionPresenter(marker).selectVenueOn(selector);
    }

    @Override
    public void replaceUserPosInicator() {
        userMarker = googleMap.addMarker(new MarkerOptions()
                .position(center()).icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_user)));
        screen().userPoiView().setVisibility(View.GONE);

    }

    @Override
    public void enableGestures() {
        this.googleMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int reason) {
                mapGesturesPresenter.onMapScrollStrarted(reason);
            }
        });

        this.googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                mapGesturesPresenter.onMapIdle(googleMap);
            }
        });

        this.googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                mapGesturesPresenter.onMarkerClick(marker, poisMarkers);
                return false;
            }
        });
    }

    @Override
    public void showUserLocationCenteredPin() {
        screen().userPoiView().setVisibility(View.VISIBLE);
    }

    @Override
    public void select(VenueAssociated v) {
        for (BMarker m : poisMarkers) {
            if (m.isAssociatedWithVenue(v.venue()))
                m.select();
            else m.unselect();
        }
    }

    @Override
    public void center(VenueAssociated venueAssociated) {
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(venueAssociated.venue().coords()));
    }

    @Override
    public void onVenueClicked(int postition, ImageView venueImage) {

    }

}
