package com.beat.test.ui.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.beat.test.data.models.Device;
import com.beat.test.ui.screens.ActivityScreen;


public class BaseActivity extends AppCompatActivity implements ActivityScreen {
    @Override
    public int code() {
        return -Integer.MAX_VALUE;
    }

    @Override
    public void setToolbarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void setToolbarIcon(int icon) {
        getSupportActionBar().setIcon(icon);
    }

    @Override
    public void homeButtonStatus(boolean enabled) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(enabled);
    }

    @Override
    public Context context() {
        return this;
    }

    @Override
    public void exit() {
        finish();
    }

    @Override
    public void homeButtonIcon(int res) {
        getSupportActionBar().setHomeAsUpIndicator(res);
    }

    @Override
    public void closeWithTransition() {
        supportFinishAfterTransition();
    }


    protected boolean areTransitionsSupported() {
        return new Device(this).isLollipop();
    }


}
