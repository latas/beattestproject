package com.beat.test.ui.screens;

public interface ActivityScreen extends Screen {

    int code();

    void setToolbarTitle(String title);

    void setToolbarIcon(int icon);

    void homeButtonStatus(boolean enabled);

    void exit();

    void homeButtonIcon(int res);

    void closeWithTransition();
}
