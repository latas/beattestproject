package com.beat.test.ui.screens;

import android.content.Context;

public interface Screen {
    Context context();
}
