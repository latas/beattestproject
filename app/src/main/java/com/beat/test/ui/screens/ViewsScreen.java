package com.beat.test.ui.screens;

import com.beat.test.data.models.State;

public interface ViewsScreen extends Screen {


    void showErrorMessage(int message);


    void transitToState(State search);

    void showLoading(String message);

    void hideLoading();
}
