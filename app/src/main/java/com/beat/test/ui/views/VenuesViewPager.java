package com.beat.test.ui.views;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;

import com.beat.test.R;
import com.beat.test.ui.screens.VenuesListScreen;

import static android.animation.ObjectAnimator.ofFloat;

/**
 * BeatTestProject-Android
 * <p>
 * Created by Antonis Latas on 9/15/2017
 */

public class VenuesViewPager extends ViewPager {

    VenuesListScreen screen;

    public VenuesViewPager(Context context) {
        super(context);
        init();
    }

    public VenuesViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void addSelectionScreen(VenuesListScreen screen) {
        this.screen = screen;
    }


    public VenuesViewPager init() {

        setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View page, float position) {
                if (getCurrentItem() == 0) {
                    page.setTranslationX(-getPaddingLeft() + getResources().getDimension(R.dimen.pager_page_margin));
                } else if (getCurrentItem() == getAdapter().getCount() - 1) {
                    page.setTranslationX(getPaddingRight() - getResources().getDimension(R.dimen.pager_page_margin));
                } else {
                    page.setTranslationX(0);
                }
            }
        });
        setTranslationY(getContext().getResources().getDimension(R.dimen.venues_list_height));
        setPageMargin((int) getResources().getDimension(R.dimen.pager_page_margin));



        addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            int previousState;
            boolean userScrollChange;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (userScrollChange)
                    screen.onVenuePageScrolled(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

                if (previousState == ViewPager.SCROLL_STATE_DRAGGING
                        && state == ViewPager.SCROLL_STATE_SETTLING)
                    userScrollChange = true;

                else if (previousState == ViewPager.SCROLL_STATE_SETTLING
                        && state == ViewPager.SCROLL_STATE_IDLE)
                    userScrollChange = false;

                previousState = state;

            }
        });


        return this;

    }

    public void show() {
        ofFloat(this, "translationY", 0).setDuration(333).start();
    }

    public void hide(final Runnable runnable) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(this, "translationY", getContext().getResources().getDimension(R.dimen.venues_list_height)).setDuration(333);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                runnable.run();
                super.onAnimationEnd(animation);
            }
        });
        animator.start();
    }


}
