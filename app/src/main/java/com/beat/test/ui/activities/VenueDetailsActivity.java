package com.beat.test.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.beat.test.R;
import com.beat.test.data.models.BImage;
import com.beat.test.presenters.ActivityPresenter;


public class VenueDetailsActivity extends BaseActivity {

    Toolbar toolbar;
    ActivityPresenter activityDecorationPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.venue_details_layout);
        if (areTransitionsSupported()) postponeEnterTransition();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ImageView imageView = (ImageView) findViewById(R.id.venue_image);
        new BImage(this).loadFillWidth(getIntent().getStringExtra(getString(R.string.venue_image_key)), imageView, new Runnable() {
            @Override
            public void run() {
                if (areTransitionsSupported())
                    startPostponedEnterTransition();
            }
        });
        activityDecorationPresenter = new ActivityPresenter(this).init();

        TextView venueName = (TextView) findViewById(R.id.venue_name);
        venueName.setText(getIntent().getStringExtra(getString(R.string.venue_name_key)));

        TextView venueType = (TextView) findViewById(R.id.venue_type);
        venueType.setText(getIntent().getStringExtra(getString(R.string.venue_type_key)));

        TextView venueAddr = (TextView) findViewById(R.id.venue_type);
        venueAddr.setText(getIntent().getStringExtra(getString(R.string.venue_type_key)));

    }

    @Override
    public int code() {
        return 1;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            activityDecorationPresenter.onHomeUpBtnPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
