package com.beat.test.ui;

import android.widget.ImageView;

import com.beat.test.data.models.VenueAssociated;

public interface VenueSelector {

    void select(VenueAssociated venueAssociated);

    void center(VenueAssociated venueAssociated);

    void onVenueClicked(int postition, ImageView venueImage);
}
