package com.beat.test.ui.views;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.TextView;

import com.beat.test.R;


public class LoadingDialog {
    private final ProgressDialog progressDialog;


    public LoadingDialog(Context context, boolean cancellable) {

        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(cancellable);
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                hide();
            }
        });
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

    }


    public LoadingDialog(Context context) {
        this(context, true);
    }


    public LoadingDialog show(String message) {
        if (progressDialog.isShowing()) {
            return this;
        }
        progressDialog.show();
        progressDialog.setContentView(R.layout.general_progress_dialog);
        TextView textView = (TextView) progressDialog.findViewById(R.id.loading_text);
        if (message == null || message.equals("")) {
            textView.setVisibility(View.GONE);
        } else textView.setText(message);
        return this;
    }

    public LoadingDialog show() {
        return this.show("");
    }

    public void hide() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }


}
