package com.beat.test.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.beat.test.R;
import com.beat.test.data.models.Venue;

import java.util.List;

/**
 * BeatTestProject-Android
 * <p>
 * Created by Antonis Latas on 9/15/2017
 */

public class VenuePagerAdapter extends FragmentStatePagerAdapter {
    final List<Venue> venues;
    final Context context;
    SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

    public VenuePagerAdapter(FragmentManager fm, Context context, List<Venue> venues) {
        super(fm);
        this.venues = venues;
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new VenueFragment();
        Bundle args = new Bundle();

        args.putString(context.getResources().getString(R.string.venue_name_key), venues.get(position).name());
        args.putString(context.getResources().getString(R.string.venue_image_key), venues.get(position).photo());
        args.putString(context.getResources().getString(R.string.venue_address_key), venues.get(position).address());
        args.putString(context.getResources().getString(R.string.venue_type_key), venues.get(position).type());

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getCount() {
        return venues.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        VenueFragment fragment = (VenueFragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }

    public int getItemPosition(Object object) {

        return POSITION_NONE;
    }
}