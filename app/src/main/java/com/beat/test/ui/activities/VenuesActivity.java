package com.beat.test.ui.activities;

import android.Manifest;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.beat.test.R;
import com.beat.test.data.models.State;
import com.beat.test.data.models.Venue;
import com.beat.test.data.models.VenueAssociated;
import com.beat.test.presenters.ActivityPresenter;
import com.beat.test.presenters.SearchPresenter;
import com.beat.test.presenters.StatePresenter;
import com.beat.test.presenters.VenueSelectionPresenter;
import com.beat.test.presenters.VenuesPresenter;
import com.beat.test.ui.VenuePagerAdapter;
import com.beat.test.ui.VenueSelector;
import com.beat.test.ui.VenuesMap;
import com.beat.test.ui.screens.MapHostingScreen;
import com.beat.test.ui.views.FakeMarkerImageView;
import com.beat.test.ui.views.LoadingDialog;
import com.beat.test.ui.views.VenuesViewPager;
import com.google.android.gms.maps.SupportMapFragment;

import java.util.Collections;
import java.util.List;

public class VenuesActivity extends BaseActivity implements MapHostingScreen, VenueSelector {

    VenuesMap map;
    TextView address;
    LoadingDialog dialog;
    private Toolbar toolbar;
    private FakeMarkerImageView userPoi;
    private FloatingActionButton searchBtn;
    private VenuesViewPager venuesPager;
    private FrameLayout addressPanel;
    private VenuesPresenter venuesPresenter;
    private StatePresenter statePresenter;
    private ActivityPresenter activityPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        dialog = new LoadingDialog(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        address = (TextView) findViewById(R.id.address_text);
        addressPanel = (FrameLayout) findViewById(R.id.address_panel);
        userPoi = ((FakeMarkerImageView) findViewById(R.id.user_poi));

        venuesPresenter = new VenuesPresenter(this, Collections.<Venue>emptyList());

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        searchBtn = (FloatingActionButton) findViewById(R.id.search_btn);

        venuesPager = (VenuesViewPager) findViewById(R.id.venues_list);
        venuesPager.addSelectionScreen(this);

        setSupportActionBar(toolbar);

        activityPresenter = new ActivityPresenter(this).init();
        map = new VenuesMap(mapFragment, this, this).init();
        statePresenter = new StatePresenter(this, map);
    }

    @Override
    public void showErrorMessage(int message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }


    @Override
    public void onBackPressed() {
        activityPresenter.onBackPressed();
    }

    @Override
    public void transitToState(State state) {
        statePresenter.reportState(state);
    }

    @Override
    public void showLoading(String message) {
        dialog.show(message);
    }

    @Override
    public void hideLoading() {
        dialog.hide();
    }

    @Override
    public void requestLocationPermission() {
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
        }, 1);
    }

    @Override
    public void updateAddress(String s) {
        address.setText(s);
        addressPanel.setVisibility(View.VISIBLE);
    }

    @Override
    public void showSearch() {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(ObjectAnimator.ofFloat(searchBtn, "scaleX", 1).setDuration(333), ObjectAnimator.ofFloat(searchBtn, "scaleY", 1).setDuration(333));
        animatorSet.start();
    }

    @Override
    public void hideSearch() {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(ObjectAnimator.ofFloat(searchBtn, "scaleX", 0).setDuration(333), ObjectAnimator.ofFloat(searchBtn, "scaleY", 0).setDuration(333));
        animatorSet.start();
    }


    @Override
    public void showVenues(List<Venue> venues) {
        venuesPager.show();
        venuesPager.setAdapter(new VenuePagerAdapter(getSupportFragmentManager(), this, venues));
        venuesPresenter = new VenuesPresenter(this, venues);
    }

    @Override
    public void scrollToItem(int i) {
        venuesPager.setCurrentItem(i);
    }

    @Override
    public void onVenueSelected(Venue venue) {
        new VenueSelectionPresenter(venue).selectVenueOn(map);
    }

    @Override
    public void navigateToDetails(Intent intent, ActivityOptionsCompat options) {
        startActivity(intent, options.toBundle());
    }

    @Override
    public void navigateToDetails(Intent intent) {
        startActivity(intent);
    }

    @Override
    public void onVenuePageScrolled(int position) {
        venuesPresenter.onVenuePageScrolled(position);
    }


    @Override
    public void hideAddressPanel() {
        addressPanel.setVisibility(View.GONE);
    }

    @Override
    public void clearVenues() {

        venuesPager.hide(new Runnable() {
            @Override
            public void run() {
                venuesPager.setAdapter(new VenuePagerAdapter(getSupportFragmentManager(), VenuesActivity.this, Collections.<Venue>emptyList()));
            }
        });
        venuesPresenter = new VenuesPresenter(this, Collections.<Venue>emptyList());

    }

    @Override
    public void convertFloatingToDismiss() {
        searchBtn.setVisibility(View.VISIBLE);
        searchBtn.setImageResource(R.drawable.close);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                statePresenter.reportState(State.MAP);
            }
        });
        ObjectAnimator anim = ObjectAnimator.ofFloat(searchBtn, "translationY", -getResources().getDimension(R.dimen.floating_button_translationY)).setDuration(150);
        anim.setStartDelay(150);
        anim.start();
    }

    @Override
    public void convertFloatingToSearch() {
        searchBtn.setVisibility(View.VISIBLE);
        searchBtn.setImageResource(R.drawable.combined_shape);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SearchPresenter(VenuesActivity.this, map, map.center()).search();
            }
        });

        ObjectAnimator.ofFloat(searchBtn, "translationY", searchBtn.getTranslationY() == 0 ? 0 : searchBtn.getTranslationY() + getResources().getDimension(R.dimen.floating_button_translationY)).setDuration(333).start();

    }

    @Override
    public ImageView userPoiView() {
        return userPoi;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            map.updateUserLocation();
        } else showErrorMessage(R.string.permission_denied);
    }


    @Override
    public int code() {
        return 0;
    }

    @Override
    public void select(VenueAssociated venueAssociated) {
    }

    @Override
    public void center(VenueAssociated venueAssociated) {
        venuesPresenter.show(venueAssociated.venue());
    }

    @Override
    public void onVenueClicked(int postition, ImageView venueImage) {
        venuesPresenter.onVenueClicked(postition, venueImage);
    }

}
