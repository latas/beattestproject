package com.beat.test.ui.screens;

import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;

import com.beat.test.data.models.Venue;

import java.util.List;

public interface VenuesListScreen extends ViewsScreen {

    /*void scrollToVenue(Venue v);*/

    void showVenues(List<Venue> venues);


    void scrollToItem(int i);

    void onVenueSelected(Venue venue);

    void navigateToDetails(Intent intent, ActivityOptionsCompat options);
    void navigateToDetails(Intent intent);

    void onVenuePageScrolled(int position);
}
