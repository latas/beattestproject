package com.beat.test.ui.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.ViewTreeObserver;

/**
 * hack for fakeImageMarker to get the same position after search with the original marker
 */
public class FakeMarkerImageView extends android.support.v7.widget.AppCompatImageView {
    public FakeMarkerImageView(Context context) {
        super(context);
        translate();
    }

    private void translate() {
        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                setTranslationY(0.33f * getHeight());//magic offset
                getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });
    }

    public FakeMarkerImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        translate();
    }

    public FakeMarkerImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        translate();
    }


}
