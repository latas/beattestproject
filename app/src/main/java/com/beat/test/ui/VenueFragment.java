package com.beat.test.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.beat.test.R;
import com.beat.test.data.models.BImage;

/**
 * BeatTestProject-Android
 * <p>
 * Created by Antonis Latas on 9/15/2017
 */

public class VenueFragment extends Fragment {
    int position = -1;
    String name, address, type, image;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.venue_fragment, container, false);


        position = getArguments().getInt(getString(R.string.fragment_pos_key));


        final ImageView imageView = (ImageView) rootView.findViewById(R.id.venue_logo);
        TextView nameTxt = (TextView) rootView.findViewById(R.id.venue_name);
        TextView addressTxt = (TextView) rootView.findViewById(R.id.venue_address);
        TextView typeTxt = (TextView) rootView.findViewById(R.id.venue_type);


        image = getArguments().getString(getResources().getString(R.string.venue_image_key));

        name = getArguments().getString(getResources().getString(R.string.venue_name_key));
        address = getArguments().getString(getResources().getString(R.string.venue_address_key));
        type = getArguments().getString(getResources().getString(R.string.venue_type_key));

        new BImage(getActivity()).load(getArguments().getString(getResources().getString(R.string.venue_image_key)), imageView);

        nameTxt.setText(name);
        addressTxt.setText(address);
        typeTxt.setText(type);


        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                ((VenueSelector) getActivity()).onVenueClicked(position, imageView);


            }
        });


        return rootView;

    }


}
