package com.beat.test.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VenuesBodyResponse {
    @SerializedName("venues")
    @Expose
    public List<VenueResponse> venueResponses;
}
