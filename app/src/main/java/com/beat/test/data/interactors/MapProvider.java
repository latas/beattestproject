package com.beat.test.data.interactors;

import com.beat.test.data.models.ResultListener;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

public class MapProvider implements Provider<GoogleMap>, OnMapReadyCallback {
    final SupportMapFragment fragment;
    ResultListener<GoogleMap> listener;

    public MapProvider(SupportMapFragment fragment) {
        this.fragment = fragment;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.listener.success(googleMap);
    }

    @Override
    public void data(ResultListener<GoogleMap> listener) {
        listener.start();
        this.listener = listener;
        fragment.getMapAsync(this);
    }
}
