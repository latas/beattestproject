package com.beat.test.data.interactors;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.beat.test.R;
import com.beat.test.data.models.ResultListener;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * BeatTestProject-Android
 * <p>
 * Created by Antonis Latas on 9/15/2017
 */

public class AddressProvider implements Provider<String> {
    final Context context;
    final LatLng latLng;


    public AddressProvider(Context context, LatLng latLng) {
        this.context = context;
        this.latLng = latLng;
    }


    @Override
    public void data(ResultListener<String> listener) {
        listener.start();
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (addresses != null && addresses.size() > 0) {
                listener.success(addresses.get(0).getAddressLine(0));
            } else listener.success(context.getString(R.string.unavailable_location));
        } catch (IOException e) {
            listener.success(context.getString(R.string.unavailable_location));
            e.printStackTrace();
        }
    }
}
