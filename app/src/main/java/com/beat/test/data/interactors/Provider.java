package com.beat.test.data.interactors;

import com.beat.test.data.models.ResultListener;

/**
 * BeatTestProject-Android
 * <p>
 * Created by Antonis Latas on 9/15/2017
 *
 */

public interface Provider<T> {
    void data(ResultListener<T> listener);
}
