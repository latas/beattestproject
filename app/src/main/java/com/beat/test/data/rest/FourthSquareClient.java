package com.beat.test.data.rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FourthSquareClient {

    final String baseUrl = "https://api.foursquare.com/v2/";

    public <S> S service(Class<S> service) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create())
                .build().create(service);
    }


}

