package com.beat.test.data.models.error;

public interface Error {
    int code();
    int message();
}
