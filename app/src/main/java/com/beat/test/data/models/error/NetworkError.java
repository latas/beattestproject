package com.beat.test.data.models.error;


import com.beat.test.R;

public class NetworkError implements Error {
    @Override
    public int code() {
        return 550;
    }

    @Override
    public int message() {
        return R.string.network_error_msg;
    }
}
