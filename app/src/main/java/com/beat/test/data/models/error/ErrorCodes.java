package com.beat.test.data.models.error;

public class ErrorCodes {

    public static int unUthorizedLocationError = 23;

    public static int locationError = 24;
    public static int noVenuesAvailable = 33;
}
