package com.beat.test.data.models.error;


import com.beat.test.R;

public class UnAuthorizedLocationError implements Error {
    @Override
    public int code() {
        return ErrorCodes.unUthorizedLocationError;
    }

    @Override
    public int message() {
        return R.string.empty;
    }
}
