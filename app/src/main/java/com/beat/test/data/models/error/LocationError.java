package com.beat.test.data.models.error;

import com.beat.test.R;

public class LocationError implements Error {
    @Override
    public int code() {
        return ErrorCodes.locationError;
    }

    @Override
    public int message() {
        return R.string.location_cannot_retrieved_message;
    }
}
