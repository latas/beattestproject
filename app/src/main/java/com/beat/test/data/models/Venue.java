package com.beat.test.data.models;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

/**
 * BeatTestProject-Android
 * <p>
 * Created by Antonis Latas on 9/15/2017
 *
 */

public class Venue implements VenueAssociated, Serializable {
    final LatLng location;
    final String name;
    final String address;
    final String photo;
    final String type;
    final String id;

    public Venue(String id, String name, LatLng location, String address, String photo, String type) {
        this.name = name;
        this.id = id;
        this.location = location;
        this.address = address;
        this.photo = photo;
        this.type = type;
    }


    public Venue(String id, String name, LatLng location, String address, String type) {
        this(id, name, location, address, new RandomVenueImage().image(), type);
    }

    public String photo() {
        return photo;
    }

    public String name() {
        return name;
    }

    public String address() {
        return address;
    }

    public String type() {
        return type;
    }


    public LatLng coords() {
        return new LatLng(location.latitude, location.longitude);
    }

    public String id() {
        return id;
    }

    public Float distanceFrom(LatLng point) {
        return new GeoDistance(location, point).meters();
    }

    @Override
    public Venue venue() {
        return this;
    }
}
