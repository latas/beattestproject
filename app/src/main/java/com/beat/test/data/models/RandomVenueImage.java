package com.beat.test.data.models;

import java.util.Random;

public class RandomVenueImage {
    //No image available in fourthsquare  search api, so use this class for a random

    final String[] images = new String[]{"https://media-cdn.tripadvisor.com/media/photo-s/0a/7b/7b/c6/restaurant-view.jpg",
            "https://bizbash-production.imgix.net/content/editorial/storyimg/big/be_leaf_11.jpg",
            "http://www.independent.com.mt/file.aspx?f=136375", "https://media-cdn.tripadvisor.com/media/photo-o/06/45/0e/fe/fino-cocktail-bar-restaurant.jpg",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQwmm1PWbKZ7OdfhZ88jMSWkHQjMKnyyVh2WPQH55oBp3yf9S5M9w",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTshfAzFqA2UymjPATV3M3leTUj_FHpONnFPUMj_iK3xbp0kAUZ", "https://media-cdn.tripadvisor.com/media/photo-s/01/a8/d0/8a/izzy-s.jpg"};

    public String image() {
        return images[new Random().nextInt(images.length)];
    }


}
