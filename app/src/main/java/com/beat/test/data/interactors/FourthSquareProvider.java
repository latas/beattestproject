package com.beat.test.data.interactors;

import com.beat.test.data.models.ResultListener;
import com.beat.test.data.models.Venue;
import com.beat.test.data.models.error.NetworkError;
import com.beat.test.data.response.FourthSquareResponse;
import com.beat.test.data.rest.FourthSquareClient;
import com.beat.test.data.rest.FourthSquareSearchParams;
import com.beat.test.data.rest.FourthSquareService;
import com.google.android.gms.maps.model.LatLng;

import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * BeatTestProject-Android
 * <p>
 * Created by Antonis Latas on 9/15/2017
 */

public class FourthSquareProvider extends SearchVenuesProvider {

    public FourthSquareProvider(LatLng center) {
        super(new FourthSquareSearchParams(center));
    }

    public FourthSquareProvider(FourthSquareSearchParams params) {
        super(params);
    }


    @Override
    public void data(final ResultListener<List<Venue>> listener) {

        FourthSquareService service = new FourthSquareClient().service(FourthSquareService.class);

        Call<FourthSquareResponse> call = service.listVenues(params.params()[0], params.params()[1], params.params()[2], params.params()[3]);

        call.enqueue(new Callback<FourthSquareResponse>() {

            @Override
            public void onResponse(Call<FourthSquareResponse> call, Response<FourthSquareResponse> response) {

                if (response.isSuccessful()) {
                    List<Venue> venues = response.body() != null ? response.body().transform() : Collections.<Venue>emptyList();
                    listener.success(venues);

                } else {
                    listener.failure(new NetworkError());
                }
            }

            @Override
            public void onFailure(Call<FourthSquareResponse> call, Throwable t) {
                listener.failure(new NetworkError());
            }
        });


        listener.start();

    }
}
