package com.beat.test.data.rest;

import com.beat.test.data.response.FourthSquareResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface FourthSquareService {

    @GET("venues/search")
    Call<FourthSquareResponse> listVenues(@Query("ll") String latLang, @Query("oauth_token") String token, @Query("v") String version, @Query("query") String query);

}
