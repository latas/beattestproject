package com.beat.test.data.interactors;

import com.beat.test.data.models.ResultListener;
import com.beat.test.data.models.SearchParams;
import com.beat.test.data.models.Venue;

import java.util.List;

public class SearchVenuesProvider implements Provider<List<Venue>> {

    final SearchParams params;

    public SearchVenuesProvider() {
        this(new SearchParams() {
            @Override
            public String[] params() {
                return new String[]{};
            }

            @Override
            public SearchParams build() {
                return this;
            }
        });
    }


    public SearchVenuesProvider(SearchParams params) {
        this.params = params.build();
    }


    @Override
    public void data(ResultListener<List<Venue>> listener) {

    }
}
