package com.beat.test.data.response;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LocationResp {

    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("lat")
    @Expose
    public double latitude;
    @SerializedName("lng")
    @Expose
    public double longitude;

    public LatLng coords() {
        return new LatLng(latitude, longitude);
    }

    public String address() {
        return address;
    }
}
