package com.beat.test.data.models;


import com.beat.test.R;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

public class BMarker implements VenueAssociated {
    final Venue venue;
    final Marker marker;
    final float initialIndex;

    public BMarker(Venue venue, Marker marker) {
        this.venue = venue;
        this.marker = marker;
        initialIndex = marker == null ? 0 : marker.getZIndex();
    }


    public BMarker(Venue venue) {
        this(venue, null);
    }

    @Override
    public Venue venue() {
        return venue;
    }

    public LatLng position() {
        return venue.coords();
    }

    public String id() {
        return marker.getId();
    }

    public void select() {
        marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.pin_active));
        marker.setZIndex(Float.MAX_VALUE);
    }

    public void unselect() {
        marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.pin));
        marker.setZIndex(initialIndex);
    }

    public void remove() {
        marker.remove();
    }

    public boolean isAssociatedWithVenue(Venue venue) {
        return venue().id().equals(venue.id());
    }
}
