package com.beat.test.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VenueCategory {
    @SerializedName("name")
    @Expose
    public String name;
}
