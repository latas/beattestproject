package com.beat.test.data.models;

import com.google.android.gms.maps.model.LatLng;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortedVenues {

    final List<Venue> venues;
    final LatLng point;


    public SortedVenues(List<Venue> venues, LatLng point) {
        this.venues = venues;
        this.point = point;
    }

    public List<Venue> sort() {
        Collections.sort(venues, new VenueComparator());
        return venues;
    }


    class VenueComparator implements Comparator<Venue> {
        public int compare(Venue c1, Venue c2) {
            return c1.distanceFrom(point).compareTo(c2.distanceFrom(point));
        }
    }
}
