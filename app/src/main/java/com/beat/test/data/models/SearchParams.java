package com.beat.test.data.models;

public interface SearchParams {
    String[] params();

    SearchParams build();
}
