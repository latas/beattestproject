package com.beat.test.data.rest;

import com.beat.test.data.models.SearchParams;
import com.google.android.gms.maps.model.LatLng;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FourthSquareSearchParams implements SearchParams {

    final LatLng point;
    String[] params;

    public FourthSquareSearchParams(LatLng point) {
        this.point = point;
    }

    String pointParam() {
        return point.latitude + "," + point.longitude;
    }

    String dateParam() {
        Date date = new Date();
        return new SimpleDateFormat("yyyyMMdd").format(date);
    }

    String token() {
        return "FY5O54JBRDF1NSAT2SQZQQGYN1QOCFY353IYWV4SCGENC15Z";
    }

    String type() {
        return "Restaurant";
    }

    @Override
    public String[] params() {
        return params;
    }

    @Override
    public SearchParams build() {
        params = new String[]{pointParam(), token(), dateParam(), type()};
        return this;
    }
}
