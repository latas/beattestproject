package com.beat.test.data.models;

/**
 * BeatTestProject-Android
 * <p>
 * Created by Antonis Latas on 9/15/2017
 *
 */

public enum State {
    INIT, MAP, SEARCH
}
