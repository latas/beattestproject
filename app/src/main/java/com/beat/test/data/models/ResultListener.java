package com.beat.test.data.models;

import com.beat.test.data.models.error.Error;

public interface ResultListener<T> {

    void start();

    void success(T t);

    void failure(Error error);
}
