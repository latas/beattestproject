package com.beat.test.data.models;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;

import android.os.Build;
import android.view.Display;

public class Device {
    final Context context;
    int width, height;

    public Device(Context context) {
        this.context = context;
    }

    public Size dimens() {
        Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
        return new Size(width, height);
    }

    public boolean isLollipop() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }


}
