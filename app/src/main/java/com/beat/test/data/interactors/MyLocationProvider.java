package com.beat.test.data.interactors;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.beat.test.data.models.error.LocationError;
import com.beat.test.data.models.LocationPermission;
import com.beat.test.data.models.ResultListener;
import com.beat.test.data.models.error.UnAuthorizedLocationError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;


public class MyLocationProvider implements Provider<Location>, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {


    final Context context;
    GoogleApiClient googleApiClient;
    ResultListener<Location> listener;


    public MyLocationProvider(Context context) {
        this.context = context;
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (new LocationPermission(context).granted()) {
            Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            if (location != null) listener.success(location);
            else listener.failure(new LocationError());
        } else {
            listener.failure(new UnAuthorizedLocationError());
        }
        googleApiClient.disconnect();

    }

    @Override
    public void onConnectionSuspended(int i) {
        listener.failure(new LocationError());
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        listener.failure(new LocationError());

    }

    @Override
    public void data(ResultListener<Location> listener) {
        this.listener = listener;
        this.listener.start();
        googleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }
}
