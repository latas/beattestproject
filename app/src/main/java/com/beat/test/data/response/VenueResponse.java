package com.beat.test.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VenueResponse {

    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("location")
    @Expose
    public LocationResp location;

    @SerializedName("categories")
    @Expose
    List<VenueCategory> categories;

    public String category() {
        return categories != null && categories.size() > 0 ? categories.get(0).name : "";
    }
}
