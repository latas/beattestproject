package com.beat.test.data.models.error;

import com.beat.test.R;

public class NoVenuesAvailableError implements Error {
    @Override
    public int code() {
        return ErrorCodes.noVenuesAvailable;
    }

    @Override
    public int message() {
        return R.string.no_venues_available;
    }
}
