package com.beat.test.data.response;

import com.beat.test.data.models.Venue;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FourthSquareResponse {


    @SerializedName("response")
    @Expose
    public VenuesBodyResponse response;

    public List<Venue> transform() {
        if (response.venueResponses == null)
            return Collections.emptyList();

        List<Venue> venues = new ArrayList<>();

        for (int i = 0; i < response.venueResponses.size(); i++)
            venues.add(new Venue(response.venueResponses.get(i).id,
                    response.venueResponses.get(i).name,
                    response.venueResponses.get(i).location.coords(),
                    response.venueResponses.get(i).location.address(),
                    response.venueResponses.get(i).category()));

        return venues;

    }

}
