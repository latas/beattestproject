package com.beat.test.data.models;

import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * BeatTestProject-Android
 * <p>
 * Created by Antonis Latas on 9/15/2017
 */

public class BImage {
    final Context context;


    public BImage(Context context) {
        this.context = context;
    }

    public void load(String path, ImageView view) {
        Picasso.with(context).load(path).into(view);
    }

    public void loadFillWidth(String path, ImageView imageView, final Runnable runnable) {

        Picasso.with(context).load(path).resize(new Device(context).dimens().width(), 0).into(imageView, new Callback() {
            @Override
            public void onSuccess() {
                runnable.run();
            }

            @Override
            public void onError() {
                runnable.run();
            }
        });
    }
}
